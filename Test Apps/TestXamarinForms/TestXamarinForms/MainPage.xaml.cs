﻿using System;
using Xamarin.Forms;

namespace TestXamarinForms
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            TabsButton.Clicked += TabsButton_Clicked;
            CarouselButton.Clicked += CarouselButton_Clicked;
            CarouselPageButton.Clicked += CarouselPageButton_Clicked;
            ButtonGridButton.Clicked += ButtonGridButton_Clicked;
            InvisiblePanelsButton.Clicked += InvisiblePanelsButton_Clicked;
            LoadDynamicDLLButton.Clicked += LoadDynamicDLLButton_Clicked;
        }

        private async void LoadDynamicDLLButton_Clicked(object sender, EventArgs e)
        {
            var app = Application.Current as App;

            if (app.TestIntGetter == null)
            {
                throw new NotImplementedException("This platform does not support dynamically loaded DLLs yet");
            }

            var theInt = await app.TestIntGetter.GetTestInt();

            await DisplayAlert("Dynamically Loaded Value", theInt.ToString(), "Cancel");
        }

        private void InvisiblePanelsButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new InvisibleTabbedPage();
        }

        private void ButtonGridButton_Clicked(object sender, EventArgs e)
        {
            var cp = new CarouselPage();
            cp.Children.Add(new ContentPage { Content = new ButtonGrid { Content = new Label { Text = "1" } }, Title = "1" });
            cp.Children.Add(new ContentPage { Content = new ButtonGrid { Content = new Label { Text = "2" } }, Title = "2" });

            Application.Current.MainPage = cp;
        }

        private void CarouselPageButton_Clicked(object sender, EventArgs e)
        {
            var cp = new CarouselPage();
            cp.Children.Add(new ContentPage { Content = new Label { Text = "1" }, Title = "1" });
            cp.Children.Add(new ContentPage { Content = new Label { Text = "2" }, Title = "2" });

            Application.Current.MainPage = cp;
        }

        private void CarouselButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new BasicCarouselViewPage();
        }

        private void TabsButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new BasicTabbedPage();
        }
    }
}
