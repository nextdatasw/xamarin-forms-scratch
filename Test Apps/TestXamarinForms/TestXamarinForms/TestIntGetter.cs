﻿using System.Threading.Tasks;

namespace TestXamarinForms
{
    public interface ITestIntGetter
    {
        Task<int> GetTestInt();
    }
}
