﻿using Monkeys.Models;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TestXamarinForms
{
    public partial class BasicCarouselViewPage : ContentPage
    {
        public BasicCarouselViewPage()
        {
            InitializeComponent();

            CarouselZoos.ItemsSource = new List<Monkey> { new Monkey { Name = "Gorilla" }, new Monkey { Name = "Chimp" } };
        }
    }
}
