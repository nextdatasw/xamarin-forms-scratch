﻿
using Xamarin.Forms;

namespace TestXamarinForms
{
    public partial class App : Application
	{
        #region Fields
        ITestIntGetter _TestIntGetter;
        #endregion

        #region Public Properties
        public ITestIntGetter TestIntGetter
        {
            get
            {
                return _TestIntGetter;
            }
        }
        #endregion

        #region Constructor
        public App (ITestIntGetter testIntGetter)
		{
            _TestIntGetter = testIntGetter;
            InitializeComponent();
			MainPage = new MainPage();
		}
        #endregion

        #region Startup Events
        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
        #endregion
    }
}
