
using System;
using System.Linq;
using System.Threading.Tasks;

namespace TestXamarinForms
{
    public partial class TestIntGetter : ITestIntGetter
    {
        #region Fields
        IFileReader _FileReader;
        #endregion

        #region Constructor
        public TestIntGetter(IFileReader fileReader)
        {
            _FileReader = fileReader;
        }
        #endregion

        #region Implementation
        public async Task<int> GetTestInt()
        {
            var assemblyBytes = await _FileReader.ReadFileAsync();

            //Load the assembly in to the app domain
            var assembly = AppDomain.CurrentDomain.Load(assemblyBytes);

            //Get a list of the types in the assembly
            var types = assembly.GetTypes();

            //Get the first type in the assembly
            var firstType = types.FirstOrDefault();

            //Create an instance of the type (TestType in my case)
            var testType = Activator.CreateInstance(firstType);

            //Get the first property on the type (TestInt in my case)
            var testIntProperty = firstType.GetProperties().FirstOrDefault();

            //Set the TestInt property to 1
            testIntProperty.SetValue(testType, 1);

            //Get the value of TestInt
            return (int)testIntProperty.GetValue(testType);
        }
        #endregion
    }
}