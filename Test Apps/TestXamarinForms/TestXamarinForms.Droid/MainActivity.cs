﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using Xamarin.Forms.Platform.Android;

namespace TestXamarinForms.Droid
{
    [Activity(Label = "TestXamarinForms", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Xamarin.Forms.Forms.Init(this, bundle);
            //LoadApplication(new App(new TestIntGetter(new WebFileReader(new Uri("http://10.0.0.75/xivicapp/DynamicTypes.dll")))));
            LoadApplication(new App(new TestIntGetter(new AssetFileReader("DynamicTypes.dll", Assets))));
        }
    }
}

