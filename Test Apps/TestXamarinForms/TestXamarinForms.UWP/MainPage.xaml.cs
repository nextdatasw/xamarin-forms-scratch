﻿namespace TestXamarinForms.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            LoadApplication(new TestXamarinForms.App(null));
        }
    }
}
