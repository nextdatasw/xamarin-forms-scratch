using System.IO;
using System.Threading.Tasks;

namespace TestXamarinForms
{
    public class FileReader : IFileReader
    {
        #region Private Fields
        private string _FileName;
        #endregion

        #region Public Properties
        public string FileName
        {
            get
            {
                return _FileName;
            }
        }
        #endregion

        #region Constructor
        public FileReader(string fileName)
        {
            _FileName = fileName;
        }
        #endregion

        #region Public Methods
        public async Task<byte[]> ReadFileAsync()
        {
            return await Task.Run(() => File.ReadAllBytes(_FileName));
        }
        #endregion
    }
}